<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');

    }

    public function index()
    {
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if($this->form_validation->run() == TRUE) {

            $email = $_POST['email'];
            $password = $_POST['password'];
            //確認user在資料庫

            $this->db->select('*');
            $this->db->form('email');
            $this->db->where(array('email' => $email, 'password' => $password));
            $query = $this->db->get();

            $email = $query->row();
            //如果存在
            if ($email->password) {

                //暫時的變量
                $this->session->set_flashdata("successful", "Your login in");

                //設定變量

                $_SESSION['email_logged'] = TRUE;
                $_SESSION['email'] = $email->email;

                //重定量

                redirect("User/admin", "refresh");

            } else {

                $this->session->set_flashdata("error", "No exits database");
                redirect("auth/login", "refresh");
            }


        }
            $this->load->view('login');
    }

132
	
}