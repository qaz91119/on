<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['email_logged'])) {

            $this->session->set_flashdata("successful", "So Amazing");
            redirect("login/login");

        }

    }
    public function  admin(){
        $this->load->view('admin');
    }


}



