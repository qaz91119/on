<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Failed</title>
    <link rel="stylesheet" href="<?php echo base_url()."dist/css/bootstrap.css" ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url()."dist/css/AdminLTE.min.css" ?>" type="text/css" >
    <style>
        .profile{
            margin: 180px auto;
            width: 450px;
            position:relative;
            border-radius:15px;
            background: #ffffff;
        }
        body{
            background-color: rgb(209,209,209);
        }

    </style>
</head>
<body>
<!-- Profile Image -->
<div class="box box-info profile">
    <div class="box box-primary">
        <div class="box-body box-profile">
            <div class ="box-header with-border">

                <h3 class="profile-username text-center"></h3>

                <p class="text-muted text-center">Failed</p>
                <?php if (isset($_SESSION['failed'])) { ?>
                    <div class="alert alert-failed">  <?php echo $_SESSION['failed']; ?></div>
                    <?php
                }
                ?>
                HELLO, <?php echo $_SESSION['failed'];?>
                <a href="#" class="btn btn-primary btn-block"><b>login out</b></a>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

</body>
</html>